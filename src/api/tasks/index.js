import axios from 'axios';

const API_URI = "http://localhost:8080/api";

export const listTasks = () => {
    return axios.get(`${API_URI}/tasks`);
};

export const searchTasks = (wordkey) => {
    return axios.get(`${API_URI}/tasks/search/${wordkey}`);
};

export const addTask = (taskData) => {
    return axios.post(`${API_URI}/tasks`, taskData);
};

export const completeTask = (taskID) => {
    return axios.put(`${API_URI}/tasks/${taskID}/complete`);
};

export const uncompleteTask = (taskID) => {
    return axios.put(`${API_URI}/tasks/${taskID}/uncomplete`);
};

export const deleteTask = (taskID) => {
    return axios.delete(`${API_URI}/tasks/${taskID}`);
};