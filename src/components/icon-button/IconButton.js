import React from 'react';
import './IconButton.css';

const IconButton = (props) => {
    return props.hide || (
        <button className={`btn btn-${props.variant}`} onClick={props.onClick}>
            <i className={`fa fa-${props.icon}`}></i>
        </button>
    );
};

export default IconButton;