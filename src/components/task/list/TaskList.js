import React from 'react';
import { connect } from 'react-redux';
import './TaskList.css';
import IconButton from "../../icon-button/IconButton";

const renderTasks = (props) => {
    const list = props.list || [];

    return list.map(task => (
        <tr key={task.id}>
            <td className={task.done ? 'maskedAsDone' : ''}>{task.description}</td>
            <td className="taskActions">
                <IconButton variant='success' icon='check' hide={task.done} onClick={() => props.handleMaskAsDone(task)}/>
                <IconButton variant='warning' icon='undo' hide={!task.done} onClick={() => props.handleMaskAsPending(task)}/>
                <IconButton variant='danger' icon='trash' hide={!task.done} onClick={() => props.handleRemove(task)}/>
            </td>
        </tr>
    ));
};

const TaskList = (props) => (
    <div>
        <table className="table tasks">
            <thead className="thead-dark">
            <tr>
                <th scope="col">Task</th>
                <th scope="col" className="taskActions">Ações</th>
            </tr>
            </thead>
            <tbody>
                {renderTasks(props)}
            </tbody>
        </table>
    </div>
);

const mapStateToProps = state => ({ list: state.tasks.list })

export default connect(mapStateToProps)(TaskList);