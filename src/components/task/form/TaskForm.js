import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import './TaskForm.css';
import Grid from "../../grid/Grid";
import IconButton from "../../icon-button/IconButton";

import { changeDescription, searchTasks, addTask } from "../../../actions";

const TaskForm = (props) => {
    const { addTask, searchTasks, description } = props;
    useEffect(() => {
        props.searchTasks();
    }, [props]);

    const keyHandler = (event) => {
        if (event.key === 'Enter') {
            event.shiftKey ? searchTasks() : addTask(description);
        }
        if (event.key === 'Escape') {
            props.onClear();
        }
    };

    return (
        <div role="form" className="todoForm">
            <div className="row">
                <Grid cols='12 9 9'>
                    <input type="text" id="description" className="form-control"
                           onChange={props.changeDescription}
                           onKeyUp={keyHandler}
                           placeholder="Adicione uma tarefa" value={props.description}/>
                </Grid>

                <Grid cols='12 3 3'>
                    <IconButton variant="primary" icon='plus' onClick={() => addTask(description)}/>
                    <IconButton variant="primary" icon='search' onClick={() => searchTasks()}/>
                    <IconButton variant="primary" icon='window-close' onClick={props.onClear}/>
                </Grid>
            </div>
        </div>
    );
};

const mapStateToProps = state => ({ description: state.tasks.description });
const mapDispatchToProps = dispatch => bindActionCreators({
    changeDescription,
    searchTasks,
    addTask
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(TaskForm);