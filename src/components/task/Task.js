import React, { Component } from 'react';
import Header from "../header/Header";
import TaskForm from "./form/TaskForm";
import TaskList from "./list/TaskList";
import {addTask, completeTask, deleteTask, listTasks, searchTasks, uncompleteTask} from "../../api/tasks";

// Component created with class style to learn
export default class Task extends Component {
    constructor(props) {
        super(props)
        this.state = {description: '', list: []};
        this.addTask = this.addTask.bind(this);
        this.handleChangeDescription = this.handleChangeDescription.bind(this);
        this.deleteTask = this.deleteTask.bind(this);
        this.completeTask = this.completeTask.bind(this);
        this.uncompleteTask = this.uncompleteTask.bind(this);
        this.searchTasks = this.searchTasks.bind(this);
        this.clear = this.clear.bind(this);

        this.refresh();
    }

    handleChangeDescription(event) {
        this.setState({ ...this.state, description: event.target.value });
    }

    refresh() {
        listTasks()
            .then(response => {
                this.setState({ ...this.state, description: '', list: response.data});
            })
            .catch(error => {
                console.log("Error: " + error);
            })
    }

    addTask() {
        addTask({description: this.state.description})
            .then(response => {
                console.log("Added!");
                this.refresh();
            })
            .catch(error => {
                console.log("Error: " + error);
            });
    }

    deleteTask(task) {
        deleteTask(task.id)
            .then(response => {
                this.refresh();
            })
            .catch(error => {
                console.log("Error: " + error);
            });
    }

    completeTask(task) {
        completeTask(task.id)
            .then(response => {
                this.refresh();
            })
            .catch(error => {
                console.log("Error: " + error);
            });
    }

    uncompleteTask(task) {
        uncompleteTask(task.id)
            .then(response => {
                this.refresh();
            })
            .catch(error => {
                console.log("Error: " + error);
            });
    }

    searchTasks() {
        let wordkey = this.state.description;

        if (wordkey === '') {
            this.refresh();
        } else {
            searchTasks(wordkey)
                .then(response => {
                    this.setState({ ...this.state, list: response.data });
                })
                .catch(error => {
                    console.log("Error: " + error);
                });
        }
    }

    clear() {
        this.refresh();
    }

    render() {
        return (
            <div>
                <Header name='Tarefas' small='Cadastro' />
                <TaskForm handleChange={this.handleChangeDescription}
                          onSearch={this.searchTasks}
                          onSubmit={this.addTask}
                          onClear={this.clear} />
                <TaskList handleMaskAsPending={this.uncompleteTask}
                          handleMaskAsDone={this.completeTask}
                          handleRemove={this.deleteTask} />
            </div>
        )
    }
}