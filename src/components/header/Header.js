import React from 'react';

import './Header.css';

const Header = (props) => (
    <header className="page-header">
        <h1>
            {props.name} <small className="text-muted header-subtitle">{props.small}</small>
        </h1>
        <hr />
    </header>
);

export default Header;