import React from 'react';
import Header from "../header/Header";

const About = (props) => (
    <div className="container">
        <Header name='About' small='Nós' />

        <h2>History</h2>
        <p>Amicitia, navis, et rumor. Living understands when you desire with solitude.</p>

        <h2>Mission and Vision</h2>
        <p>Moon, modification, and mystery. Strudel tastes best with mayonnaise and lots of marmalade.</p>

        <h2>Impress</h2>
        <p>Moon, modification, and mystery. Strudel tastes best with mayonnaise and lots of marmalade.</p>

    </div>
);

export default About;