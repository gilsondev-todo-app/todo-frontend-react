import React from 'react';

const toCSSClasses = (numbers) => {
    const cols = numbers ? numbers.split(' ') : [];
    let classes = '';

    if(cols[0]) classes += `col-xs-${cols[0]} `;
    if(cols[1]) classes += `col-sm-${cols[1]} `;
    if(cols[2]) classes += `col-md-${cols[2]} `;
    if(cols[3]) classes += `col-lg-${cols[3]}`;

    return classes;
};

const gridClasses = (props) => {
    return toCSSClasses(props.cols || 12);
};

const Grid = (props) => (
    <div className={gridClasses(props)}>
        {props.children}
    </div>
);

export default Grid;