import React from 'react';
import {Link, NavLink} from "react-router-dom";

const Menu = (props) => (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <Link className="navbar-brand" to="/tasks">
            <i className="fa fa-calendar-check-o"></i> Todo App
        </Link>

        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div className="navbar-nav">
                <NavLink className="nav-item nav-link" activeClassName="active" to="/tasks">Tasks</NavLink>
                <NavLink className="nav-item nav-link" activeClassName="active" to="/about">About</NavLink>
            </div>
        </div>
    </nav>
);

export default Menu;
