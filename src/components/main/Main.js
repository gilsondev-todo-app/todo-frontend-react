import React from 'react';
import Task from "../task/Task";
import About from "../about/About";
import {Redirect, Route, Switch} from "react-router-dom";

const Main = (props) => (
    <div className="container">
        <Switch>
            <Route path='/tasks' component={Task} />
            <Route path='/about' component={About} />
            <Redirect from='*' to='/tasks' />
        </Switch>
    </div>
);

export default Main;
