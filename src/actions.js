import axios from 'axios';

const URL = 'http://localhost:8080/api';

export const changeDescription = event => ({
   type: 'DESCRIPTION_CHANGED',
   payload: event.target.value
});

export const searchTasks = () => {
   const request = axios.get(`${URL}/tasks`);
   return {
      type: 'TASKS_SEARCHED',
      payload: request
   }
};

export const addTask = description => {
   const request = axios.post(`${URL}/tasks`, { description });
   return {
      type: 'TASK_ADDED',
      payload: request
   }
};