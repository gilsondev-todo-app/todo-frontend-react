import React from 'react';
import './App.css';
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import promise from 'redux-promise';
import rootReducer from "./reducers";

import Menu from "./components/menu/Menu";
import Main from "./components/main/Main";

const devTools = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();
const store = applyMiddleware(promise)(createStore)(rootReducer, devTools);

const App = (props) => (
    <Provider store={store}>
        <div className="container">
            <Menu />
            <Main />
        </div>
    </Provider>
);

export default App;
