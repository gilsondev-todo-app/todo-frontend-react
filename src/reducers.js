import { combineReducers } from "redux";

const INITIAL_STATE = { description: '', list: [] };

const taskReducer = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case 'DESCRIPTION_CHANGED':
            return { ...state, description: action.payload};
        case 'TASKS_SEARCHED':
            return { ...state, list: action.payload.data };
        case 'TASK_ADDED':
            return { ...state, description: '' };
        default:
            return state
    }
};

const rootReducer = combineReducers({
    tasks: taskReducer
});

export default rootReducer;
